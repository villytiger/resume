%% vtresume.cls
%% Copyright 2020 Ilya Lyubimov
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `author-maintained'.
%
% This work consists of the file vtresume.cls.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{vtresume}[2020/03/08 VillyTiger Resume Class]
\LoadClass[a4paper]{extarticle}

\RequirePackage[left=1.25cm,right=1.25cm,top=1.5cm,bottom=1.5cm,columnsep=1.2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[default]{lato}

\RequirePackage{fontawesome5}
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{paracol}
\RequirePackage[skins,raster]{tcolorbox}
\RequirePackage{dashrule}
\RequirePackage{tabularx}
\RequirePackage{pbox}

\RequirePackage{vtutil}

\pagestyle{empty}
\setlength{\parindent}{0pt}
\pretolerance10000
\exhyphenpenalty10000
\frenchspacing

\definecolor{@purple}{HTML}{3E0097}
\colorlet{heading}{@purple}
\colorlet{accent}{@purple}
\colorlet{emphasis}{black}
\colorlet{verylight}{black!30}
\colorlet{light}{black!60}

\newcommand\@location{\faMapMarker*}
\newcommand\@calendar{\faCalendar*[regular]}

\newcommand\cvName[1]{\def\@cvName{#1}}
\newcommand\cvPosition[1]{\def\@cvPosition{#1}}
\newcommand\cvLocation[1]{\def\@cvLocation{#1}}

\newcommand\cvContact[3][https://]{\vtListPushBack\@contacts{{#1}{#2}{#3}}}
\newcommand\cvEmail[1]{\cvContact[mailto:]{#1}{\faAt}}
\newcommand\cvLinkedin[1]{\cvContact{#1}{\faLinkedin}}
\newcommand\cvGithub[1]{\cvContact{#1}{\faGithub}}
\newcommand\cvGitlab[1]{\cvContact{#1}{\faGitlab}}

\newcommand\cvSummary[1]{\def\make@summary{\make@section{Summary}#1}}
\newcommand\cvExperience[1]{\vtListPushBack\exp@list{#1}}
\newcommand\cvEducation[1]{\def\edu@val{#1}}
\newcommand\cvCourse[1]{\vtListPushBack\course@list{#1}}
\newcommand\cvLanguage[2]{\vtListPushBack\lang@list{{#1}{#2}}}
\newcommand\cvTechnologies[1]{\vtListPushBack\tech@list{#1}}

\newcommand\cvSourceCode[1]{\def\source@code{#1}}


\newcommand\make@divider[1][\bigskip]{%
  \hrule height 0pt%
  #1%
  \textcolor{black!50}{\hdashrule[0pt][x]{\linewidth}{0.5pt}{0.5ex}}%
  #1%
  \hrule height 0pt%
}


\newcommand\make@section[1]{
  \begingroup
  \par\bigskip
  \color{heading}
  {\LARGE\bfseries\MakeUppercase{#1}}
  \vspace{1ex}
  \hrule height 2pt
  \par\bigskip
  \endgroup
}


\newcommand\make@event[3]{%
  \begingroup
  {\large\color{emphasis}#1}\par\smallskip
  {\normalsize\bfseries\color{accent}#2}\par\smallskip
  {\small\color{light}\makebox[0.5\linewidth][l]{\@calendar\hspace{0.5em}#3}}
  \endgroup
}


\newtcbox\@tagbox{
  colback=white,colframe=verylight,colupper=light,
  boxrule=0.5pt,boxsep=0.5em,left=0pt,right=0pt,height=2em,
  valign=center,nobeforeafter
}

\newcommand\make@tags[1]{
  \pbox{\linewidth}{
    \spaceskip=0.5em
    \baselineskip=2.5em
    \raggedright
    \vtListJoin{ }{\vtListMap\@tagbox{\vtlist#1}}
  }
}


\newcommand\exp@project@do[3]{
  \makebox[3em]{\textcolor{accent}{#1}}\textbf{#2}
  \par\smallskip
  \textcolor{light}{#3}
}

\newcommand\exp@project[1]{\exp@project@do#1}

\newcommand\exp@handle[1]{
  \begingroup

  \newcommand\position[1]{\def\exp@position{##1}}
  \newcommand\organization[1]{\def\exp@organization{##1}}
  \newcommand\when[1]{\def\exp@when{##1}}
  \newcommand\location[1]{\def\exp@location{##1}}
  \newcommand\summary[1]{\def\exp@summary{##1}}
  \newcommand\project[3]{\vtListPushBack\exp@projects{{##1}{##2}{##3}}}

  #1

  \begingroup
  \setlength{\tabcolsep}{0pt}
  \footnotesize\color{light}
  \begin{tabularx}{\linewidth}{XcX}
    {\large\textcolor{emphasis}{\exp@position}} & \@calendar & \hspace{0.5em}\exp@when \\[4pt]
    {\normalsize\bfseries\textcolor{accent}{\exp@organization}} & \@location & \hspace{0.5em}\exp@location
  \end{tabularx}
  \endgroup

  \ifdefined\exp@summary
  \medskip
  \exp@summary
  \fi

  \ifdefined\exp@projects
  \medskip
  \vtListJoin{\par\medskip}{\vtListMap\exp@project\exp@projects}
  \fi

  \endgroup
}

\newcommand\make@experience[0]{
  \make@section{Experience}
  \vtListJoin\make@divider{\vtListMap\exp@handle\exp@list}
}


\newcommand\make@technologies[0]{
  \make@section{Technologies}
  \vtListJoin\make@divider{\vtListMap\make@tags\tech@list}
}


\newcommand\lang@handle@do[2]{%
\textcolor{emphasis}{\textbf{#1}}\hfill
\foreach \x in {1,...,5}{%
  \space{\ifnumgreater{\x}{#2}{\color{verylight}}{\color{accent}}\faCircle}}\par%
}

\newcommand\lang@handle[1]{\lang@handle@do#1}

\newcommand\make@languages[0]{
  \make@section{Languages}
  \vtListJoin{\par\smallskip}{\vtListMap\lang@handle\lang@list}
}


\newcommand\make@education[0]{
  \make@section{Education}

  \begingroup
  \newcommand\degree[1]{\def\edu@degree{##1}}
  \newcommand\organization[1]{\def\edu@organization{##1}}
  \newcommand\when[1]{\def\edu@when{##1}}

  \edu@val

  \make@event{\edu@degree}{\edu@organization}{\edu@when}

  \endgroup
}


\newcommand\course@handle[1]{
  \begingroup
  \newcommand\name[1]{\def\course@name{##1}}
  \newcommand\organization[1]{\def\course@organization{##1}}
  \newcommand\when[1]{\def\course@when{##1}}

  #1

  \make@event{\course@name}{\course@organization}{\course@when}

  \endgroup
}

\newcommand\make@courses[0]{
  \make@section{Courses}
  \vtListJoin\make@divider{\vtListMap\course@handle\course@list}
}


\newcommand\make@contact@do[3]{\href{#1#2}{\makebox[2em]{\textcolor{accent}{#3}}\textcolor{light}{#2}}}

\newcommand\make@contact[1]{\make@contact@do#1}

\newcommand\make@header[0]{
  \begingroup
  \baselineskip=0pt\bfseries
  \begin{paracol}{2}
    {\Huge\color{emphasis}\MakeUppercase{\@cvName}}\par\medskip
    {\large\color{accent}\@cvPosition}\par\medskip
    {\footnotesize\textcolor{accent}{\@location}\hspace{0.5em}\@cvLocation}
    \switchcolumn
    \small\raggedleft\pbox{\linewidth}{
      \vtListJoin{\\[3pt]}{\vtListMap\make@contact\@contacts}
    }
  \end{paracol}
  \endgroup
}


\newcommand\makeCv[0]{
  \begin{document}

  \make@header

  \medskip

  \columnratio{0.6}
  \begin{paracol}{2}

    \make@summary
    \make@experience

    \switchcolumn

    \make@education
    \make@courses
    \make@languages
    \make@technologies

  \end{paracol}

  \vfill\raggedleft\color{verylight}\footnotesize
  Source code: \href{https://\source@code}{\source@code}

  \end{document}
}
